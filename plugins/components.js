import Vue from "vue"
import VueObserveVisibility from 'vue-observe-visibility'
import Footer from '~/components/footer'
import blockHeader from '~/components/blockHeader'
import specialBlock from '~/components/specialBlock'
import gallery from '~/components/gallery'

Vue.use(VueObserveVisibility);
Vue.config.productionTip = false;
Vue.component('main-footer', Footer);
Vue.component('block-header', blockHeader);
Vue.component('special-block', specialBlock);
Vue.component('gallery', gallery);