import axios from "axios";

export default {
  state() {
    return {
      galleries: null
    };
  },
  getters: {
    
  },
  mutations: {
    setGalleries(state, data) {
      state.galleries = data;
    }
  },
  actions: {
    loadGalleries(vuexContext) {
      let config = {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + vuexContext.rootState.auth.token
        }
      };
      axios
        .get(vuexContext.rootState.auth.baseUrl + "/feed/galleries", config)
        .then(response => {
          console.log("ovo su galerije");
          console.log(response);
          vuexContext.commit("setGalleries", response.data.galleries);
        })
        .catch(er => alert(er));
    }
  }
};
