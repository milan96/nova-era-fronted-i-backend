import axios from 'axios'
import Cookie from 'js-cookie'

export default {
  state() {
    return {
      token: null,
      userUUID: null,
      userDetail: null,
      baseUrl: null
    }
  },
  getters: {
    isAuth(state) {
      return state.token != null
    }
  },
  mutations: {
    token(state, data) {
      state.token = data
    },
    userUUID(state, data) {
      state.userUUID = data
    },
    userDetail(state, data) {
      state.userDetail = data
    },
    setBaseUrl(state, data) {
      state.baseUrl = data
    },
    clearToken(state) {
      state.token = null
      if (process.client) {
        localStorage.removeItem('token')
        localStorage.removeItem('tokenExpiration')
      }

      Cookie.remove('jwt')
      Cookie.remove('expirationDate')
      console.log('Odjava')
    }
  },
  actions: {
    async login(vuexContext, data) {
      let config = {
        headers: { 'Content-Type': 'application/json' }
      }
      axios
        .post(vuexContext.state.baseUrl + '/auth/login', data, config)
        .then(response => {
          console.log(response.data)

          vuexContext.commit('token', response.data.token)
          vuexContext.commit('userUUID', response.data.userId)

          localStorage.setItem('token', response.data.token)
          localStorage.setItem('userUUID', response.data.userId)
          localStorage.setItem(
            'tokenExpiration',
            new Date().getTime() + response.data.expiresIn
          )

          Cookie.set('jwt', response.data.token)
          Cookie.set(
            'expirationDate',
            new Date().getTime() + response.data.expiresIn
          )
          Cookie.set('userUUID', response.data.userId)
        })
        .catch(error => {
          console.log(error)
        })
    },
    loadBaseUrl(vuexContext) {
      console.log('proces')

      let development = process.env.NODE_ENV === 'development'

      if (development == true) {
        let baseUrl = '/api'
        vuexContext.commit('setBaseUrl', baseUrl)
      } else {
        let baseUrl = '/api'
        vuexContext.commit('setBaseUrl', baseUrl)
      }
    },
    initAuth(vuexContext, req) {
      let token
      let expirationDate
      let userUUID
      if (req) {
        if (!req.headers.cookie) {
          return
        }
        const jwtCookie = req.headers.cookie
          .split(';')
          .find(c => c.trim().startsWith('jwt='))
        if (!jwtCookie) {
          return
        }
        token = jwtCookie.split('=')[1]

        expirationDate = req.headers.cookie
          .split(';')
          .find(c => c.trim().startsWith('expirationDate='))
          .split('=')[1]

        userUUID = req.headers.cookie
          .split(';')
          .find(c => c.trim().startsWith('userUUID='))
          .split('=')[1]
      } else {
        token = localStorage.getItem('token')
        expirationDate = localStorage.getItem('tokenExpiration')
        userUUID = localStorage.getItem('userUUID')
      }
      if (new Date().getTime() > expirationDate || token === null) {
        vuexContext.commit('clearToken')
      }
      vuexContext.commit('token', token)
      vuexContext.commit('userUUID', userUUID)
    },
    logout(vuexContext) {
      vuexContext.commit('clearToken')
    },
    loadUserInfo(vuexContext, req) {
      console.log('Izvrsava se')
      if (vuexContext.state.userUUID !== null) {
        let config = {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + vuexContext.state.token
          }
        }
        axios
          .get(
            vuexContext.state.baseUrl +
              '/auth/userinfo/?uuid=' +
              vuexContext.state.userUUID,
            config
          )
          .then(response => {
            console.log('ovde se izvrsava')
            vuexContext.commit('userDetail', response.data.user)
          })
          .catch(er => alert(er))
      }
    }
  }
}
