const express = require("express");
const { body } = require("express-validator/check");
const multer = require("multer");

const isAuth = require("../middleware/is-auth");

const storage = multer.diskStorage({
  destination: function(req, res, cb) {
    cb(null, __dirname.replace("routes", "") + "images/");
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});
const feedController = require("../controllers/feed");

const router = express.Router();

// GET /feed/posts
router.get("/posts", feedController.getPosts);

router.get("/spcificPosts", feedController.getSpcificPosts);

router.get("/users", feedController.getUsers);

// POST /feed/post
router.post(
  "/post",
  isAuth,
  [
    body("title")
      .trim()
      .isLength({ min: 5 }),
    body("content")
      .trim()
      .isLength({ min: 5 })
  ],
  upload.single("image"),
  feedController.createPost
);

router.get("/post", feedController.getPost);

router.put(
  "/post/:postID",
  isAuth,
  upload.single("image"),
  feedController.updatePost
);

router.delete("/post/:postID", isAuth, feedController.deletePost);

//Ovde ide sve sto treba za galerije
router.get("/galleries", feedController.getGalleries);

router.post(
  "/gallery",
  isAuth,
  feedController.createGallery
);


module.exports = router;
