const express = require("express");
const { body } = require("express-validator/check");

const User = require("../models/user");
const authController = require('../controllers/auth')

const router = express.Router();

router.post("/signup", [
  body("email").withMessage("Unesite ispravan email").custom((value, {req}) => {
      return User.findOne({email: value}).then(userDoc => {
          if(userDoc) {
              return Promise.reject('Ovaj email se vec koristi!')
          }
      })
  })
  .normalizeEmail(),
  body('password').trim().isLength({mi: 8}),
  body('name').trim().not().isEmpty()
], authController.signup);

router.post('/login', authController.login)

router.get('/userinfo', authController.loadUser)

module.exports = router;
