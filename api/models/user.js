const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  userID: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  imageUrl: { type: String, default: "/images/avatar.png" },
  userType: {
    type: String,
    default: "user"
  },
  posts: [
    {
      type: Schema.Types.ObjectId,
      ref: "Post"
    }
  ],
  galleries: [
    {
      type: Schema.Types.ObjectId,
      ref: "Gallery"
    }
  ],
  articles: [
    {
      type: Schema.Types.ObjectId,
      ref: "Article"
    }
  ]
});

module.exports = mongoose.model("User", userSchema);
