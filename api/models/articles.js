const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const postSchema = new Schema({
  articleID: { type: String, required: true },
  title: { type: String, required: true },
  content: { type: String, required: true },
  dateCreated: { type: String, required: true },
  imageUrl: { type: String, required: true },
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
});

module.exports = mongoose.model("Article", postSchema);