const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const gallerySchema = new Schema({
  galleryID: { type: String, required: true },
  title: { type: String, required: true },
  items: { type: Array, required: true },
  originalAutor: { type: String, required: false },
  dateCreated: { type: String, required: true },
  creator: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
});

module.exports = mongoose.model("Gallery", gallerySchema);