const fs = require("fs");
const path = require("path");

const { validationResult } = require("express-validator/check");
const Post = require("../models/post");
const User = require("../models/user");
const Gallery = require("../models/gallery");

exports.getPosts = (req, res, next) => {
  Post.find()
    .then(posts => {
      res.status(200).json({ message: "Ucitane su objave", posts: posts });
    })
    .catch(err => {
      if (err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
  // Post.find(function(err, foundPosts) {
  //   if (err) {
  //     res.send(err);
  //   } else {
  //     res.send(foundPosts);
  //   }
  // });
};

exports.getUsers = (req, res, next) => {
  User.find()
    .then(users => {
      res.status(200).json({ message: "Ucitani su korisnici", users: users });
    })
    .catch(err => {
      if (err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
  // Post.find(function(err, foundPosts) {
  //   if (err) {
  //     res.send(err);
  //   } else {
  //     res.send(foundPosts);
  //   }
  // });
};

exports.createPost = (req, res, next) => {
  const errors = validationResult(req);
  // if (!errors.isEmpty()) {
  //   const error = new Error("Losi podaci");
  //   error.statusCode = 422;
  //   throw error;
  // }
  if (!req.file) {
    const error = new Error("Nema slike");
    error.status = 422;
    throw error;
  }
  // console.log(req.file);

  const imageUrl = req.file.path.split("api/")[1];

  const title = req.body.title;
  const content = req.body.content;
  const postID = req.body.postID;
  const shortContent = req.body.shortContent;
  const category = req.body.category;
  const dateCreated = req.body.dateCreated;
  let creator;
  // console.log(req.body);

  const post = new Post({
    title: title,
    content: content,
    postID: postID,
    imageUrl: imageUrl,
    creator: req.userId,
    shortContent: shortContent,
    dateCreated: dateCreated,
    category: category
    // imageUrl: imageUrl
  });
  post
    .save()
    .then(result => {
      return User.findById(req.userId);
    })
    .then(user => {
      creator = user;
      user.posts.push(post);
      return user.save();
    })
    .then(result => {
      res.status(201).json({
        message: "Objava je kreirana",
        post: post,
        creator: { _id: creator._id, name: creator.name }
      });
    })
    .catch(err => {
      console.log(err);
    });
};

exports.getPost = (req, res, next) => {
  const postID = req.query.postID;

  Post.findOne({ postID: postID })
    .then(post => {
      if (!post) {
        const error = new Error("Nema takve objave");
        error.statusCode = 404;
        throw error;
      }
      res.status(200).json({ message: "Ucitani su postovi", post: post });
    })
    .catch(err => {
      if (err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });

  // Post.findOne({ postID })
  //   .then(post => {
  //     if (!post) {
  //       const error = new Error("Nema takve objave");
  //       error.statusCode = 404;
  //       throw error;
  //     }
  //     res.status(200).json({ message: "Ucitani su postovi", post: post });
  //   })
  //   .catch(err => {
  //     if (err.statusCode) {
  //       err.statusCode = 500;
  //     }
  //     next(err);
  //   });
};

exports.updatePost = (req, res, next) => {
  const postID = req.params.postID;
  console.log(postID);
  // const errors = validationResult(req);
  // if (!errors.isEmpty()) {
  //   const error = new Error('Validation failed, entered data is incorrect.');
  //   error.statusCode = 422;
  //   throw error;
  // }
  console.log(req.body);

  const title = req.body.title;
  const content = req.body.content;
  const shortContent = req.body.shortContent;
  const category = req.body.category;
  // const dateCreated = req.body.dateCreated;
  console.log(req.file);

  let imageUrl;
  if (req.file) {
    imageUrl = req.file.path.split("api/")[1];
  } else {
    imageUrl = req.body.imageUrl;
  }
  // if (!imageUrl) {
  //   const error = new Error("No file picked.");
  //   error.statusCode = 422;
  //   throw error;
  // }
  Post.findOne({ postID })
    .then(post => {
      if (!post) {
        const error = new Error("Could not find post.");
        error.statusCode = 404;
        throw error;
      }
      if (imageUrl !== post.imageUrl) {
        clearImage(post.imageUrl);
      }
      post.title = title;
      post.imageUrl = imageUrl;
      post.content = content;
      post.shortContent = shortContent;
      post.category = category;
      return post.save();
    })
    .then(result => {
      res.status(200).json({ message: "Post updated!", post: result });
    })
    .catch(err => {
      if (!err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
};

const clearImage = filePath => {
  filePath = path.join(filePath);
  fs.unlink(filePath, err => console.log(err));
};

exports.deletePost = (req, res, next) => {
  console.log("Pocinje brisanje");

  const postID = req.params.postID;
  console.log(req.params);

  Post.findOneAndDelete({ postID })
    .then(result => {
      console.log(result);
      res.status(200).json({ message: "Објава је успешно обрисана" });
    })
    .catch(err => {
      console.log(err);
    });
  // Post.findOne({postID})
};

exports.getSpcificPosts = (req, res, next) => {
  let numberOfPosts = Number(req.query.number);
  let query = req.query.category;
  let current = req.query.current;

  if (query) {
    if (current) {
      Post.find({ category: query, postID: { $ne: current } })
        .then(posts => {
          res.status(200).json({
            message: "Ucitane su objave",
            posts: posts
              .slice()
              .reverse()
              .slice(0, numberOfPosts)
          });
        })
        .catch(err => {
          if (err.statusCode) {
            err.statusCode = 500;
          }
          next(err);
        });
    } else {
      Post.find({ category: query })
        .then(posts => {
          res.status(200).json({
            message: "Ucitane su objave",
            posts: posts
              .slice()
              .reverse()
              .slice(0, numberOfPosts)
          });
        })
        .catch(err => {
          if (err.statusCode) {
            err.statusCode = 500;
          }
          next(err);
        });
    }
  } else {
    Post.find()
      .then(posts => {
        res.status(200).json({
          message: "Ucitane su objave",
          posts: posts
            .slice()
            .reverse()
            .slice(0, numberOfPosts)
        });
      })
      .catch(err => {
        if (err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      });
  }
};

//Galerije

exports.createGallery = (req, res, next) => {
  console.log('radi ovde pravljenje');
  console.log(req.body);
  console.log('asdasdasdasdasdasdasokdimaoskdmoasmdokasmdo');
      
      console.log(req.userId);
  
  
  const title = req.body.title;
  const items = req.body.items;
  const galleryID = req.body.galleryID;
  const originalAutor = req.body.originalAutor ? req.body.originalAutor : null;
  const dateCreated = req.body.dateCreated;
  let creator;

  const gallery = new Gallery({
    title: title,
    items: items,
    galleryID: galleryID,
    originalAutor: originalAutor,
    creator: req.userId,
    dateCreated: dateCreated,
  });
  gallery
    .save()
    .then(result => {
      console.log('ovovovovovov');
      
      return User.findById(req.userId);
    })
    .then(user => {
      console.log('nadjen');
      console.log(user);
      
      
      creator = user;
      user.galleries.push(gallery);
      return user.save();
    })
    .then(result => {
      res.status(201).json({
        message: "Галерија је креирана",
        gallery: gallery,
        creator: { _id: creator._id, name: creator.name }
      });
    })
    .catch(err => {
      console.log(err);
    });
};

exports.getGalleries = (req, res, next) => {
  console.log('Ovo se pokrece');
  
  Gallery.find()
    .then(galleries => {
      res.status(200).json({ message: "Ucitane su galerije", galleries: galleries });
    })
    .catch(err => {
      if (err.statusCode) {
        err.statusCode = 500;
      }
      next(err);
    });
  // Post.find(function(err, foundPosts) {
  //   if (err) {
  //     res.send(err);
  //   } else {
  //     res.send(foundPosts);
  //   }
  // });
};