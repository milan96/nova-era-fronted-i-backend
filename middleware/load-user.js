export default function(context) {
    if (context.store.getters['auth/isAuth']) {
        context.store.dispatch('auth/loadUserInfo')
    }
}